# FileUploadTest

Pruebas de subidas de ficheros a Liferay.

+ Subidas a directorio temporal de sistema (comprobar variable en linux):
    * Con formulario: un fichero, un fichero con JS, múltiples ficheros con JS.
    * Por Ajax: un fichero, múltiples ficheros, múltiples ficheros con drag&drop.
+ Subidas a directorio del repositorio de documentos de Liferay (API DlAppServiceUtil)
+ Listado de contenido de carpeta de sistema con descarga mediante RessourceCommand.
+ Listado de carpeta del repositorio de Liferay y obtención de enlaces de descarga.

**Nota:** Requiere desplegar [commons-io-2.6.jar](http://commons.apache.org/proper/commons-io/download_io.cgi).