<%@ include file="/init.jsp" %>

<liferay-ui:error key="noFile" message="No has seleccionado un fichero para subir"/>
<liferay-ui:error key="fichGrande" message="El fichero supera el tama�o permitido de 10 MB"/>
<liferay-ui:error key="noTipo" message="Solo se admiten ficheros en formato PDF"/>
<liferay-ui:error key="errorCopiarFichero" message="Error al copiar el fichero"/>

<h2>Subir m�ltiples ficheros con comprobaci�n JS</h2>

<portlet:actionURL name="dlAppFileUpload" var="fileForm" />

<portlet:renderURL var="cancelar">
	<portlet:param name="mvcPath" value="/view.jsp"/>
</portlet:renderURL>


<script>

function comprobarFichero(event){
	console.log(event);
	
	var inputField = document.getElementById('mifile');
    
	var selectedFiles = inputField.files;
	console.log(selectedFiles);

	for(var i=0; i<selectedFiles.length; i++) {
		var file = selectedFiles[i];
		console.log(file);
		}
	
}

function comprobarCampos(){
	var error = false;
	files = document.getElementById('mifile').files;
	if(files == "" || files == undefined || files.length < 1){
		alert("Selecciona un fichero");
		error = true;
	}else{
		for(f in files){
			fichero = files[f];
			if(fichero instanceof File){
				if(fichero.type != "application/pdf"){
					alert("Seleccione un fichero pdf");
					error = true;
				}
				if(fichero.size > (10*1024*1024)){
					alert("El tama�o del fichero supera el m�ximo permitido de 10 MB");
					error = true;
				}
			}
		}
	}
	
	if(!error){
		$("#<portlet:namespace/>miform").submit();
	}
}
</script>


<form id="<portlet:namespace/>miform" action='<%=fileForm%>' method="POST" enctype="multipart/form-data" >
	<div class="form-group">
		<label for="<portlet:namespace/>file">Selecciona varios archivos</label>
		<input class="form-control" type="file" name="<portlet:namespace/>file" id="mifile" multiple="multiple"/>
		<p class="help-block">Selecciona archivos pdf de menos de 10MB.</p>
	</div>
	<input class="btn btn-default" type="button" name="<portlet:namespace/>submit" value="Enviar" id="<portlet:namespace/>submit" onclick="comprobarCampos()"></input>
	<a class="btn btn-secondary" href="<%=cancelar%>">Cancelar</a>
</form>
<div id="ficherosSeleccionados">
</div>

