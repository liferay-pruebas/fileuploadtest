<%@ include file="/init.jsp"%>

<portlet:resourceURL id='multipleFileUpload' var='ajaxUploadURL' />

<portlet:renderURL var="cancelar">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<h3>AUI Form</h3>
<aui:form name="myForm" enctype="multipart/form-data">
	<aui:input type="file" name="myFile" label="My File" multiple="true"></aui:input>
	<aui:button type="submit" name="btnUploadFile" value="Upload File AUI"></aui:button>
</aui:form>

<h3>JQuery Form</h3>
<form id="<portlet:namespace />form2" enctype="multipart/form-data" action="#">
	<div class="form-group">
		<label for="<portlet:namespace/>file">Selecciona varios archivos</label>
		<input class="form-control" type="file" name="<portlet:namespace />myFile" multiple="true" />
		<p class="help-block">Selecciona archivos pdf de menos de 10MB.</p>
	</div>
	<input class="btn btn-default" type="button" id="mibtn" value="Upload File JQuery">
</form>

<a class="btn btn-secondary" href="<%=cancelar%>">Cancelar</a>

<span id="response"></span>

<script>
AUI().use(
	    'aui-io-request',
	    function(A){
	    	var btnUploadFile = A.one("#<portlet:namespace />btnUploadFile");

	    	btnUploadFile.on("click", uploadFile);

	    	function uploadFile(event){
	    	    event.preventDefault();
	    	    var myForm = A.one("#<portlet:namespace/>myForm");
	    	    var ajaxURL = "<%=ajaxUploadURL%>";
	    	    
	    	    var configs = {
	    	        method: 'POST',
	    	        form: {
	    	            id: myForm,
	    	            upload: true
	    	        },
	    	        sync: true,
	    	        on: {
	    	            complete: function(){
	    	            	alert(response);
	    	            }
	    	        }
	    	    };
	    	    
	    	    A.io.request(ajaxURL, configs);    
	    	}
	});

$(document).ready(function(){
	$("#mibtn").click(function(event){
		event.preventDefault();
		
		// Get form
        var form = $('#<portlet:namespace/>form2')[0];

		// Create an FormData object 
        var data = new FormData(form);
		
		request = $.ajax({
            url: "<%=ajaxUploadURL%>",
			type : "post",
			data : data,
			processData : false,
			contentType : false
		//?no probado
		});

		// Callback handler that will be called on success
		request.done(function(response, textStatus, jqXHR) {
			// Log a message to the console
			console.log("Hooray, it worked!");
			alert("Ficheros subidos con �xito");
			//$( "#response" ).text('AJAX Response: ' + response);
			//alert(response);
		});

		// Callback handler that will be called on failure
		request.fail(function(jqXHR, textStatus, errorThrown) {
			// Log the error to the console
			console.error("The following error occurred: "
					+ textStatus, errorThrown);

			alert("Fallo al subir los ficheros");
		});
	});
});
</script>