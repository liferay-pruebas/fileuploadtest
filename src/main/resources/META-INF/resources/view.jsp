<%@ include file="/init.jsp" %>

<liferay-ui:success key="success" message="Fichero subido con �xito" />

<portlet:renderURL var="formSubida">
	<portlet:param name="mvcPath" value="/form.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="formSubidaHtml">
	<portlet:param name="mvcPath" value="/formhtml5.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="formSubidaHtmlMultFich">
	<portlet:param name="mvcPath" value="/formhtml5multfich.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="formSubidaAjax">
	<portlet:param name="mvcPath" value="/subidaAjaxForm.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="formSubidaAjaxMultFich">
	<portlet:param name="mvcPath" value="/subidaAjaxFormMultfich.jsp"/>
</portlet:renderURL>

<portlet:renderURL var="formSubidaAjaxMultFichSelector">
	<portlet:param name="mvcPath" value="/subidaAjaxFormMultfichSelector.jsp"/>
</portlet:renderURL>


<portlet:renderURL var="formSubidaHtmlMultFichToLFFolder">
	<portlet:param name="mvcPath" value="/formhtml5multfichToLFFolder.jsp"/>
</portlet:renderURL>
<h2>Pruebas de formularios:</h2>
<ul>
<li><a href="<%= formSubida %>">Subir fichero</a></li>
<li><a href="<%= formSubidaHtml %>">Subir fichero con comprobaci�n JS</a></li>
<li><a href="<%= formSubidaHtmlMultFich %>">Subir varios ficheros con comprobaci�n JS</a></li>
<li><a href="<%= formSubidaAjax %>">Subir fichero por Ajax</a></li>
<li><a href="<%= formSubidaAjaxMultFich %>">Subir varios ficheros por Ajax</a></li>
<li><a href="<%= formSubidaAjaxMultFichSelector %>">Subir varios ficheros por Ajax con selector JS</a></li>
<li><a href="<%= formSubidaHtmlMultFichToLFFolder %>">Subir varios ficheros al gestor de doc de liferay</a></li>
</ul>

<h3>Ficheros subidos al directorio del sistema:</h3>
<%
	List<String> lista = (List<String>) request.getAttribute("ficheros");
	if(lista!=null){
%>
	<ul>
	<%for(String file:lista){ %>
		
		<li><a href="<portlet:resourceURL id='fileDownload'><portlet:param name='fileName' value='<%= file%>'/></portlet:resourceURL>"><%= file%></a></li>
	<%} %>
	</ul>
<%} %>


<h3>Contenido de la carpeta raiz del rep. de documentos de liferay:</h3>
<%
	List<String> listaRaiz = (List<String>) request.getAttribute("contenidoRaiz");
	if(listaRaiz!=null){
%>
	<ul>
	<%for(String file:listaRaiz){ %>
		
		<li><%= file%></li>
	<%} %>
	</ul>
<%} %>

<h3>Ficheros subidos a la carpeta prueba en el rep. de doc. de liferay con enlaces:</h3>
<%
	Map<String,String> contenidoPrueba = (Map<String,String>) request.getAttribute("contenidoPrueba");
	if(contenidoPrueba!=null){
%>
	<ul>
	<%for(String key:contenidoPrueba.keySet()){ %>
		
		<li><a href='<%=contenidoPrueba.get(key) %>'><%= key%></a></li>
	<%} %>
	</ul>
<%} %>
