<%@ include file="/init.jsp" %>

<liferay-ui:error key="noFile" message="No has seleccionado un fichero para subir"/>
<liferay-ui:error key="fichGrande" message="El fichero supera el tama�o permitido de 10 MB"/>
<liferay-ui:error key="noTipo" message="Solo se admiten ficheros en formato PDF"/>
<liferay-ui:error key="errorCopiarFichero" message="Error al copiar el fichero"/>

<h2>Subir fichero</h2>

<portlet:actionURL name="fileUpload" var="fileForm" />

<portlet:renderURL var="cancelar">
	<portlet:param name="mvcPath" value="/view.jsp"/>
</portlet:renderURL>

<form action='<%=fileForm%>' method="POST" enctype="multipart/form-data" >
	<input type="hidden" name="<portlet:namespace/>from" value="/form.jsp"/>
	<div class="form-group">
		<label for="<portlet:namespace/>file">Selecciona un archivo</label>
		<input class="form-control" type="file" name="<portlet:namespace/>file"/>
		<p class="help-block">Selecciona un archivo pdf de menos de 10MB.</p>
	</div>
	<button type="submit" class="btn btn-default">Submit</button>
	<a class="btn btn-secondary" href="<%=cancelar%>">Cancelar</a>
</form>