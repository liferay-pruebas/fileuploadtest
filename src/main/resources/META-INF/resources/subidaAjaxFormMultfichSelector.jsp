<%@ include file="/init.jsp" %>

<portlet:resourceURL id='multipleFileUpload' var='ajaxUploadURL'/>

<portlet:renderURL var="cancelar">
	<portlet:param name="mvcPath" value="/view.jsp" />
</portlet:renderURL>

<style>
	#filedrag{
		display: none;
		font-weight: bold;
		text-align: center;
		padding: 1em;
		margin 1em;
		color: #555;
		border: 2px dashed #555;
		border-radius: 7px;
		cursor: default;
	}
	
	#filedrag.hover{
		color: #f00;
		border-color: #f00;
		border-style: solid;
		box-shadow: inset 0 3px 4px #888
	}
</style>

<script>
var ficheros = [];

function existe(file){
	for(i in ficheros){
		f = ficheros[i];
		if(f != undefined && f != null && f.name == file.name) return true;
	}
	return false;
}

function comprobarFichero(file){
	correcto = true;
	if(file instanceof File){
		if(file.type != "application/pdf"){
			alert("Seleccione un fichero pdf");
			correcto = false;
		}
		if(file.size > (10*1024*1024)){
			alert("El tama�o del fichero supera el m�ximo permitido de 10 MB");
			correcto = false;
		}
	}
	return correcto;
}

function resetFileInput(){
	input = document.getElementById("mifile");
	input.value = '';

	if(!/safari/i.test(navigator.userAgent)){
		input.type = '';
		input.type = 'file';
	}
}

function anadirFicheros(files){
	for(f in files){
		fichero = files[f];
		if(comprobarFichero(fichero) && !existe(fichero)){
			ficheros.push(fichero);
		}
	};
	listarFicheros();
	resetFileInput();
}

function fileDropListener(event){
	fileDragHover(event);
	
	var files = event.target.files || event.dataTransfer.files;
	
	anadirFicheros(files);
}

function fileDragHover(e){
	e.stopPropagation();
	e.preventDefault();
	e.target.className = (e.type == "dragover" ? "hover" : "");
}

function fileListener(event){
	files = document.getElementById('mifile').files;
	if(files == "" || files == undefined || files.length < 1){
		//nada
	}else{
		anadirFicheros(files);
	}
}

function listarFicheros(){
	if(ficheros != "" || ficheros != undefined || ficheros.length > 0){
		fileDiv = document.getElementById("fileList");
		fileDiv.innerHTML = "<ul>";
		for(i in ficheros){
			if(ficheros[i].name && ficheros[i].name != "item"){
				fileDiv.innerHTML += '<li>' + ficheros[i].name + '<input class="btn btn-danger btn-sm" type="button" value="eliminar" onclick="eliminarFichero(' + i + ')"/></li>';
			}
		};
		fileDiv.innerHTML += "</ul>";
	}
}

function eliminarFichero(i){
	
	delete ficheros[i];
	
	listarFicheros();
}

function enviarFormulario(){
	var data = new FormData();
	for(i in ficheros){
		f = ficheros[i];
		if(f != undefined && f != null){
			data.append("<portlet:namespace />myFile", f);
		}
	};
	
	/* console.log(data);
	for (var i in data){
		p = data[i];
		console.log(p);
	}
	 */
	request = $.ajax({
        url: "<%=ajaxUploadURL%>",
        type: "post",
        data: data,
        processData:false,contentType:false//?no probado
    });
	
	// Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log("Hooray, it worked!");
        alert("Ficheros subidos con �xito");
        //$( "#response" ).text('AJAX Response: ' + response);
        //alert(response);
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );

        alert("Fallo al subir los ficheros");
    });
}
</script>
<form id="miform" action='#' method="POST" enctype="multipart/form-data" >
	<div class="form-group">
		<label for="<portlet:namespace/>file">Selecciona varios archivos</label>
		<input class="form-control" type="file" name="file" id="mifile" onchange="fileListener()" multiple accept="application/pdf"/>
		<p class="help-block">Selecciona archivos pdf de menos de 10MB.</p>
	</div>
	<div id="filedrag" style="width:100%; height:70px">Arrastra ficheros aqu�</div>
	<input class="btn btn-default" type="button" name="Submit" onclick="enviarFormulario()" value="Enviar"/>
	<a class="btn btn-secondary" href="<%=cancelar%>">Cancelar</a>
</form>
<div id="fileList"></div>

<script type="text/javascript">

$(document).ready(function(){
	
	filedrag = document.getElementById("filedrag");
	filedrag.addEventListener("dragover", fileDragHover, false);
	filedrag.addEventListener("dragleave", fileDragHover, false);
	filedrag.addEventListener("drop", fileDropListener, false);
	filedrag.style.display = "block";
});
</script>

