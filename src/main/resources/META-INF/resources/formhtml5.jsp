<%@ include file="/init.jsp" %>

<liferay-ui:error key="noFile" message="No has seleccionado un fichero para subir"/>
<liferay-ui:error key="fichGrande" message="El fichero supera el tama�o permitido de 10 MB"/>
<liferay-ui:error key="noTipo" message="Solo se admiten ficheros en formato PDF"/>
<liferay-ui:error key="errorCopiarFichero" message="Error al copiar el fichero"/>

<h2>Subir fichero con comprobaci�n JS</h2>

<portlet:actionURL name="fileUpload" var="fileForm" />

<portlet:renderURL var="cancelar">
	<portlet:param name="mvcPath" value="/view.jsp"/>
</portlet:renderURL>


<script>

function comprobarFichero(event){
	console.log(event);
	
	var inputField = document.getElementById('mifile');
    
	var selectedFiles = inputField.files;
	console.log(selectedFiles);

	for(var i=0; i<selectedFiles.length; i++) {
		var file = selectedFiles[i];
		console.log(file);
		}
	
}

function comprobarCampos(){
	var error = false;
	files = document.getElementById('mifile').files;
	if(files == "" || files == undefined || files.length < 1){
		alert("Selecciona un fichero");
		error = true;
	}else{
		fichero = files[0];
		if(fichero.type != "application/pdf"){
			alert("Seleccione un fichero pdf");
			error = true;
		}
		if(fichero.size > (10*1024*1024)){
			alert("El tama�o del fichero supera el m�ximo permitido de 10 MB");
			error = true;
		}
	}
	
	if(!error){
		$("#<portlet:namespace/>miform").submit();
	}
}
</script>


<form id="<portlet:namespace/>miform" action='<%=fileForm%>' method="POST" enctype="multipart/form-data" >
	<input type="hidden" name="<portlet:namespace/>from" value="/formhtml5.jsp"/>
	<div class="form-group">
		<label for="<portlet:namespace/>file">Selecciona un archivo</label>
		<input class="form-control" type="file" name="<portlet:namespace/>file" id="mifile"/>
		<p class="help-block">Selecciona un archivo pdf de menos de 10MB.</p>
	</div>
	<input class="btn btn-default" type="button" name="<portlet:namespace/>submit" value="Enviar" id="<portlet:namespace/>submit" onclick="comprobarCampos()"></input>
	<a class="btn btn-secondary" href="<%=cancelar%>">Cancelar</a>
</form>

