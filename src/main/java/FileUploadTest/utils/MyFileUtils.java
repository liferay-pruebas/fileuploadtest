package FileUploadTest.utils;

import java.io.File;

import javax.portlet.PortletRequest;

import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.PropsUtil;

public class MyFileUtils {
	
	private static String lfUploadFolder = "prueba";
	private static String lfUploadFolderDesc = "Directorio de pruebas de subidas de ficheros desde portlet";
	
	private static Folder folder;

	public static String getStorePath(){
		String lfDataDir = "data";
		String uploadDir = "subidasLF";
		String path = PropsUtil.get("liferay.home");
		
		String storeDir = path+"/"+lfDataDir+"/"+uploadDir;
		File dir = new File(storeDir);
		
		if(!dir.exists())
			System.out.println("Dir creado?" + dir.mkdirs() + " ruta: " + dir.getAbsolutePath());
		
		return storeDir;
	}
	
	/**
	 * Comprueba que la carpeta lfUploadFolder existe en la ra�z de documentos de LF.
	 * La crea si no existe.
	 * @param renderRequest
	 * @param themeDisplay
	 * @throws PortalException 
	 */
	public static void assertUploadFolderExists(PortletRequest portletRequest, ThemeDisplay themeDisplay) throws PortalException {
		long repositoryId = themeDisplay.getScopeGroupId();
		long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
		ServiceContext serviceContext = null;
		try {
			serviceContext = ServiceContextFactory.getInstance(DLFolder.class.getName(), portletRequest);
			folder = DLAppServiceUtil.getFolder(repositoryId, parentFolderId, lfUploadFolder);
		} catch (NoSuchFolderException e){
			System.out.println(e.getMessage());
			System.out.println("No existe el directorio en la raiz de documentos... creando...");
			DLAppServiceUtil.addFolder(repositoryId, parentFolderId, lfUploadFolder, lfUploadFolderDesc, serviceContext);
		} catch (SystemException e1) {
			System.out.println(e1.getMessage());
			e1.printStackTrace();
		}
	}
	
	/**
	 * Devuelve el directorio del repo LF usado en las pruebas
	 * @return null si no existe o no ha sido previamente comprobado
	 */
	public static Folder getLFFolder(){
		return folder;
	}
}
