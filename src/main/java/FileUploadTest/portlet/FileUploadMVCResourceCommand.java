package FileUploadTest.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.URLConnection;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import FileUploadTest.constants.FileUploadTestPortletKeys;
import FileUploadTest.utils.MyFileUtils;

@Component(
		property = { 
				"javax.portlet.name=" + FileUploadTestPortletKeys.FileUploadTest,
				"mvc.command.name=fileUpload" }, 
		service = MVCResourceCommand.class)
public class FileUploadMVCResourceCommand implements MVCResourceCommand {
	
	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		UploadPortletRequest upReq = PortalUtil.getUploadPortletRequest(resourceRequest);
		boolean error = false;
		try {
			System.out.println("Procesando la subida de un fichero");
			String name = upReq.getFileName("myFile");
			File f = upReq.getFile("myFile");
			
			//Para mensajes de respuesta
			resourceResponse.setContentType("text/plain");
			resourceResponse.resetBuffer();
			PrintWriter resp = resourceResponse.getWriter();		
			
			if (name == null || name.equals("")) {
				System.out.println("No hay ficheros");
				resp.println("No hay ficheros que procesar");
			} else {
				String mimeType = MimeTypesUtil.getContentType(f);
				long size = upReq.getSize("myFile");// Tama�o en Bytes
				System.out.println("Fichero: " + name + " tama�o: " + size + " ruta: " + f.getAbsolutePath());

				if (!mimeType.equals("application/pdf")) {
					error = true;
					resp.println("El fichero no est� en formato pdf");
				}
				if (size > 10 * 1024 * 1024) {
					error = true;
					resp.println("El fichero supera el tama�o permitido de 10MB");
				}

				if (!error) {
					// copiar fichero a la carpeta
					File copy = new File(MyFileUtils.getStorePath(), System.nanoTime() + name);
					if (copy.createNewFile()) {
						FileInputStream in = new FileInputStream(f);
						FileOutputStream out = new FileOutputStream(copy);
						IOUtils.copy(in, out);
						resp.println("Fichero guardado correctamente");
					} else {
						error = true;
						resp.println("Error al copiar un fichero");
					}
				}
			}
			resourceResponse.flushBuffer();//Cerrar respuesta
			return error;
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {
			
		}
		return error;
	}

}
