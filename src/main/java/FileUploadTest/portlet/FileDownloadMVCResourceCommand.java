package FileUploadTest.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URLConnection;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.Validator;

import FileUploadTest.constants.FileUploadTestPortletKeys;
import FileUploadTest.utils.MyFileUtils;

@Component(
		property = { 
				"javax.portlet.name=" + FileUploadTestPortletKeys.FileUploadTest,
				"mvc.command.name=fileDownload" }, 
		service = MVCResourceCommand.class)
public class FileDownloadMVCResourceCommand implements MVCResourceCommand {
	
	@Override
	public boolean serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws PortletException {
		String fileName = resourceRequest.getParameter("fileName");
		File outputFile = new File(MyFileUtils.getStorePath(), fileName);
		
		if(fileName == null){
			resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE, "400");
			return true;
		}
		
		System.out.println("file path: " + outputFile.getAbsolutePath());
		
		OutputStream out = null;
		InputStream in = null;
		try {
			if(outputFile.exists()){
				//Servir el fichero
				FileNameMap fileNameMap = URLConnection.getFileNameMap();
			    String mimeType = MimeTypesUtil.getContentType(outputFile);
			    
				resourceResponse.setContentType(mimeType);
				resourceResponse.addProperty("Content-disposition", "atachment; filename=" + fileName);
				
				
				out = resourceResponse.getPortletOutputStream();
				in = new FileInputStream(outputFile);
				IOUtils.copy(in, out);	
			}else{
				//No se ha encontrado el archivo
				resourceResponse.setProperty(ResourceResponse.HTTP_STATUS_CODE, "404");
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (Validator.isNotNull(out)) {
					out.flush();
					out.close();
				}
				if (Validator.isNotNull(in)) {
					in.close();
				}
				return false;

			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

}
