package FileUploadTest.portlet;

import FileUploadTest.constants.FileUploadTestPortletKeys;
import FileUploadTest.utils.MyFileUtils;

import com.liferay.document.library.kernel.exception.NoSuchFolderException;
import com.liferay.document.library.kernel.model.DLFileEntry;
import com.liferay.document.library.kernel.model.DLFolder;
import com.liferay.document.library.kernel.model.DLFolderConstants;
import com.liferay.document.library.kernel.service.DLAppServiceUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.service.ServiceContext;
import com.liferay.portal.kernel.service.ServiceContextFactory;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.HtmlUtil;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.PortalUtil;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.PortletRequest;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

/**
 * @author dramirez
 */
@Component(property = { "com.liferay.portlet.display-category=category.sample", "com.liferay.portlet.instanceable=true",
		"javax.portlet.display-name=FileUploadTest Portlet", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + FileUploadTestPortletKeys.FileUploadTest,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class FileUploadTestPortlet extends MVCPortlet {

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {

		List<String> ficheros = new ArrayList<>();

		File dir = new File(MyFileUtils.getStorePath());
		String[] fich = dir.list();
		if (fich != null) {
			for (String s : fich) {
				ficheros.add(s);
			}
		}

		renderRequest.setAttribute("ficheros", ficheros);
		
		try {
			// sacar params
			Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
			ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);
			Long repositoryId = themeDisplay.getScopeGroupId();
						
			//Asegurar que la carpeta de subidas al repo de LF existe
			MyFileUtils.assertUploadFolderExists(renderRequest, themeDisplay);
			
			// Listar los ficheros y carpetas de la carpeta raiz del sistema de
			// documentos de liferay
			List<String> contenidoRaiz = new ArrayList<>();
			List<Folder> folders = DLAppServiceUtil.getFolders(repositoryId, parentFolderId);
			for (Folder f : folders) {
				contenidoRaiz.add("Carpeta " + f.getName());
			}
			List<FileEntry> fileEntries = DLAppServiceUtil.getFileEntries(repositoryId, parentFolderId);
			for (FileEntry f : fileEntries) {
				contenidoRaiz.add("Fichero " + f.getFileName());
			}
			
			renderRequest.setAttribute("contenidoRaiz", contenidoRaiz);

			// Listar los ficheros de la carpeta prueba con enlaces
			// de descarga

			Folder prueba = MyFileUtils.getLFFolder();
			List<FileEntry> pruebaEntries = DLAppServiceUtil.getFileEntries(repositoryId, prueba.getFolderId());
			Map<String, String> mapUrls = new TreeMap<>();
			
			for(FileEntry f:pruebaEntries){
				//!!!!!!!!!!Codificar el nombre del archivo para usar en la URL por si tiene caracteres especiales
				String url = themeDisplay.getPortalURL() + themeDisplay.getPathContext() + "/documents/" + 
							 themeDisplay.getScopeGroupId() + "/" + f.getFolderId() +  "/" + HtmlUtil.escapeURL(HtmlUtil.unescape(f.getTitle()));
				mapUrls.put(f.getFileName(), url);
			}
			
			renderRequest.setAttribute("contenidoPrueba", mapUrls);
			
		} catch (PortalException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}

		super.render(renderRequest, renderResponse);
	}

	/**
	 * Subida de un fichero a carpeta del sistema
	 * @param request
	 * @param response
	 */
	public void fileUpload(ActionRequest request, ActionResponse response) {
		UploadPortletRequest upReq = PortalUtil.getUploadPortletRequest(request);
		
		//Se llama desde 2 pags diferentes, obtengo de la que vengo por par�metro
		String fromPage = request.getParameter("from");
		try {
			System.out.println("Procesando la subida de un fichero");
			String name = upReq.getFileName("file");
			File f = upReq.getFile("file");

			if (name == null || name.equals("")) {
				System.out.println("No hay ficheros");
				SessionErrors.add(request, "noFile");
				response.setRenderParameter("mvcPath", fromPage);
			} else {
				String mimeType = MimeTypesUtil.getContentType(f);
				long size = upReq.getSize("file");// Tama�o en Bytes
				System.out.println("Fichero: " + name + " tama�o: " + size + " ruta: " + f.getAbsolutePath());

				boolean error = false;
				if (!mimeType.equals("application/pdf")) {
					SessionErrors.add(request, "noTipo");
					error = true;
				}
				if (size > 10 * 1024 * 1024) {
					SessionErrors.add(request, "fichGrande");
					error = true;
				}

				if (error) {
					response.setRenderParameter("mvcPath", fromPage);
				}else{
					// copiar fichero a la carpeta
					File copy = new File(MyFileUtils.getStorePath(), System.nanoTime() + name);
					System.out.println("Subido a: " + copy.getAbsolutePath());
					if (copy.createNewFile()) {
						FileInputStream in = new FileInputStream(f);
						FileOutputStream out = new FileOutputStream(copy);
						IOUtils.copy(in, out);
						SessionMessages.add(request, "success");
					} else {
						SessionErrors.add(request, "errorCopiarFichero");
						response.setRenderParameter("mvcPath", fromPage);
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {

		}
	}

	/**
	 * Subida de m�ltiples ficheros a carpeta del sistema
	 * @param request
	 * @param response
	 */
	public void multipleFileUpload(ActionRequest request, ActionResponse response) {
		UploadPortletRequest upReq = PortalUtil.getUploadPortletRequest(request);
		String fromPage = "formhtml5multfich.jsp";

		try {
			System.out.println("Procesando la subida de multiples ficheros");

			Map<String, FileItem[]> map = upReq.getMultipartParameterMap();

			if (map.isEmpty()) {
				System.out.println("No hay ficheros");
				SessionErrors.add(request, "noFile");
				response.setRenderParameter("mvcPath", fromPage);
			} else {
				boolean error = false;
				for (FileItem file : map.get("file")) {

					String mimeType = MimeTypesUtil.getContentType(file.getStoreLocation());
					long size = file.getSize();// Tama�o en Bytes
					System.out.println("Fichero: " + file.getFileName() + " tama�o: " + size + " ruta: "
							+ file.getStoreLocation().getPath());

					if (!mimeType.equals("application/pdf")) {
						SessionErrors.add(request, "noTipo");
						error = true;
					}
					if (size > 10 * 1024 * 1024) {
						SessionErrors.add(request, "fichGrande");
						error = true;
					}
				}

				if (error) {
					response.setRenderParameter("mvcPath", fromPage);
				} else {
					ThemeDisplay themeDisplay = (ThemeDisplay) request.getAttribute(WebKeys.THEME_DISPLAY);
					//Asegurar que la carpeta para subidas existe
					MyFileUtils.assertUploadFolderExists( request, themeDisplay);
					
					// Copiar los ficheros a la carpeta
					List<File> copiados = new LinkedList<>();
					error = false;
					for (FileItem file : map.get("file")) {
						File copy = new File(MyFileUtils.getStorePath(), System.nanoTime() + file.getFileName());
						if (copy.createNewFile()) {
							FileInputStream in = new FileInputStream(file.getStoreLocation());
							FileOutputStream out = new FileOutputStream(copy);
							IOUtils.copy(in, out);
							copiados.add(copy);
						} else {
							error = true;
							break;
							// TODO borrar copiados (sube todos o ninguno)
						}
					}
					if (!error) {
						SessionMessages.add(request, "success");
					} else {
						SessionErrors.add(request, "errorCopiarFichero");
						response.setRenderParameter("mvcPath", fromPage);
					}
				}
			}
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} catch (PortalException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {

		}

	}

	/**
	 * DlApp file upload. Subida de m�ltiples ficheros al sistema de documentos de liferay
	 * @param request
	 * @param response
	 */
	public void dlAppFileUpload(ActionRequest request, ActionResponse response) {
		UploadPortletRequest upReq = PortalUtil.getUploadPortletRequest(request);
		String fromPage = "/formhtml5multfichToLFFolder.jsp";

		try {
			System.out.println("Procesando la subida de multiples ficheros");
			Map<String, FileItem[]> map = upReq.getMultipartParameterMap();

			if (map.isEmpty()) {
				System.out.println("No hay ficheros");
				SessionErrors.add(request, "noFile");
				response.setRenderParameter("mvcPath", fromPage);
			} else {
				//Recoger y comprobar los ficheros recibidos
				boolean error = false;
				for (FileItem file : map.get("file")) {

					String mimeType = MimeTypesUtil.getContentType(file.getStoreLocation());
					long size = file.getSize();// Tama�o en Bytes
					System.out.println("Fichero: " + file.getFileName() + " tama�o: " + size + " ruta: "
							+ file.getStoreLocation().getPath());

					if (!mimeType.equals("application/pdf")) {
						SessionErrors.add(request, "noTipo");
						error = true;
					}
					if (size > 10 * 1024 * 1024) {
						SessionErrors.add(request, "fichGrande");
						error = true;
					}
				}

				if (error) {
					response.setRenderParameter("mvcPath", fromPage);
				} else {
					// Copiar los ficheros a la carpeta
					List<FileEntry> copiados = new LinkedList<>();
					error = false;
					for (FileItem file : map.get("file")) {
						String mimeType = MimeTypesUtil.getContentType(file.getStoreLocation());
						String title = System.currentTimeMillis() + file.getFileName();
						String description = "Content added via portlet";
						String changeLog = "hi";
						Long parentFolderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
						try {
							ServiceContext serviceContext = ServiceContextFactory
									.getInstance(DLFileEntry.class.getName(), request);
							long repositoryId = serviceContext.getScopeGroupId();
							Folder folder = MyFileUtils.getLFFolder();

							InputStream is = file.getInputStream();
							copiados.add(DLAppServiceUtil.addFileEntry(repositoryId, folder.getFolderId(), title, mimeType, title,
									description, changeLog, is, file.getSize(), serviceContext));
						} catch (Exception e) {
							System.out.println(e.getMessage());
							e.printStackTrace();
						}
					}
					if (!error) {
						SessionMessages.add(request, "success");
					} else {
						SessionErrors.add(request, "errorCopiarFichero");
						response.setRenderParameter("mvcPath", fromPage);
					}
				}

			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		} finally {

		}
	}
	
}