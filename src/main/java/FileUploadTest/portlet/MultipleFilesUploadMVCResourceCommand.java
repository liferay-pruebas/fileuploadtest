package FileUploadTest.portlet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.apache.commons.io.IOUtils;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.portlet.bridges.mvc.BaseMVCResourceCommand;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCResourceCommand;
import com.liferay.portal.kernel.servlet.SessionErrors;
import com.liferay.portal.kernel.servlet.SessionMessages;
import com.liferay.portal.kernel.upload.FileItem;
import com.liferay.portal.kernel.upload.UploadPortletRequest;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.PortalUtil;

import FileUploadTest.constants.FileUploadTestPortletKeys;
import FileUploadTest.utils.MyFileUtils;

@Component(
		property = { 
				"javax.portlet.name=" + FileUploadTestPortletKeys.FileUploadTest,
				"mvc.command.name=multipleFileUpload" }, 
		service = MVCResourceCommand.class)
public class MultipleFilesUploadMVCResourceCommand implements MVCResourceCommand {

	@Override
	public boolean serveResource(ResourceRequest request, ResourceResponse response)
			throws PortletException {
		UploadPortletRequest upReq = PortalUtil.getUploadPortletRequest(request);
		boolean error = false;
		//Mensajes de respuesta
		StringBuilder resp = new StringBuilder();
		String fileFieldName = "myFile";
		
		try{
			System.out.println("Procesando la subida de multiples ficheros");
			
			Map<String, FileItem[]> map = upReq.getMultipartParameterMap();
			
			if(!hayFicheros(fileFieldName, map)){
				System.out.println("No hay ficheros");
				error = true;
				resp.append("No hay ficheros que cargar\n");
			}
			else{
				FileItem[] files = map.get(fileFieldName);
				for(FileItem file:files){
					String mimeType = MimeTypesUtil.getContentType(file.getStoreLocation());
					long size = file.getSize();//Tama�o en Bytes
					System.out.println("Fichero: "+ file.getFileName() +" tama�o: " + size + " ruta: " + file.getStoreLocation().getPath());
					
					if(mimeType == null || !mimeType.equals("application/pdf")){
						error = true;
						resp.append("El fichero no es de tipo pdf\n");
						//System.out.println("El fichero no es de tipo pdf " + error);
					}
					
					if(size > 10*1024*1024){
						error = true;
						resp.append("El fichero supera el tama�o permitido de 10MB\n");
						//System.out.println("El fichero supera el tama�o permitido de 10MB " + error);
					}
				}
					
				
				if(!error){
					//Copiar los ficheros a la carpeta
					List<File> copiados = new LinkedList<>();
					for(FileItem file:files){
						File copy = new File(MyFileUtils.getStorePath(), System.nanoTime() + file.getFileName());
						if(copy.createNewFile()){
							FileInputStream in = new FileInputStream(file.getStoreLocation());
							FileOutputStream out = new FileOutputStream(copy);
							IOUtils.copy(in, out);
							copiados.add(copy);
						}else{
							error = true;
							break;
							//TODO borrar copiados (sube todos o ninguno
						}
					}
					if(!error){
						resp.append("Se han subido los ficheros");
					}else{
						resp.append("Error al copiar ficheros");
					}
				}
				
			}
			
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			error = true;
		} catch (IOException e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			error = true;
		}finally{
			try {
				System.out.println("Bloque finally");
				response.setContentType("text/plain");
				response.resetBuffer();
				response.reset();
				System.out.println("Error es: " + error);
				if(error){
					response.setProperty(ResourceResponse.HTTP_STATUS_CODE, "415");
				}
				response.getWriter().print(resp.toString());;
				response.flushBuffer();
			} catch (IOException e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
			}
		}
		return error;
	}

	private boolean hayFicheros(String name, Map<String, FileItem[]> map) {
		if(map.get(name) == null || map.get(name).length == 0){
			System.out.println("Mapa vacio, sin ficheros");
			return false;
		}
		
		for(FileItem f:map.get(name)){
			if(f.getSize() != 0){
				System.out.println("Mapa con ficheros");
				return true;
			}
		}
		System.out.println("Mapa con ficheros de tama�o 0");
		return false;
	}

}
